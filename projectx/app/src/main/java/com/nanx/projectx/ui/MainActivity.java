package com.nanx.projectx.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.nanx.projectx.Adapter.DataPhotoAdapter;
import com.nanx.projectx.R;
import com.nanx.projectx.object.Data;
import com.nanx.projectx.service.FakeService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {


    //sempre deixe seus objetos globais da tela instanciados
    private RecyclerView recyclerView;
    private BottomNavigationView bottomNavigationView;
    private DataPhotoAdapter adapter;
    private List<Data> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //esta chamada é padrao, estas duas linhas sempre devem ocorrer
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //vincula as view com o xml
        recyclerView = findViewById(R.id.recyckerViewList);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);

        //setamos a configuraçao de nossa bottonNavigation
        //podemos passar desse modo como apresentadoa qui na linha 50
        //pois estamos implementando a bottomnavigation view em nossa classe
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        //

        //mas podemos fazer desse modo:
        /*
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                //e implementamos aqui nosso switch case, do mesmo modo
                //mas assim deixa o codigo um pouco mais poluido visualmente
                return false;
            }
        });
*/

        //instancia o retrofit
        Retrofit retrofit = new Retrofit.Builder()
                //passamos o link de acesso
                //e nossos endpoints ficam dentro de nosso service
                .baseUrl("https://jsonplaceholder.typicode.com/")
                //adicionamos um convereter para facilicar nossa vida
                // isto permite que pulamos uma etapa de conversao de json para nosso object
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        //basicamente uma interface tem a funcao e e realizar a comunicação entre 2 sistemas
        //neste caso utilizamos uma interface para comunicar o retrofic com nosso sistema
        //por padrao do retrofit os endpoint sao definidos dentro de uma interface
        //utilizando de notações especiais do retrofit (@get, @put... etc)

        /* instancia a interface FakeService*/
        FakeService service = retrofit.create(FakeService.class);
        // instanciamos a chamada
        Call<List<Data>> repos = service.listPhotos();

        //e realizamos a chamada, a funcao enqueue é uma funcao do retrofit
        // ela faz uma chamada assincrona e notifica sobre o retorno da chamada
        // para isso chamanos a funcao e passamos um callback para retorno
        repos.enqueue(new Callback<List<Data>>() {
            @Override
            public void onResponse(Call<List<Data>> call, Response<List<Data>> response) {

                System.out.println(response);

                //passamos nosso retorno para a função a fim de popular nosso recyclerview
                //como nosso call possui o tipo do formato "List<Data>", a resposta da nossa consulta se torna do formato tipo "List<Data>"
                //logo podemos pegar diretamente nossa lista pela funcao response.body()
                populateRecyclerView(response.body());

            }

            //em caso de falha, url nao encontrada, algum problema, a funcao onFailure captura esse erro
            // e podemos apresestar este erro e / ou realizar algum tratamento sobre ele
            @Override
            public void onFailure(Call<List<Data>> call, Throwable t) {

                Toast.makeText(MainActivity.this, "Falha na consulta: "+ t.getCause(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void tesate(){

    }




    private void populateRecyclerView(List<Data> photoList) {

        //setamos nosso adapter
        adapter = new DataPhotoAdapter(this, photoList);
        //passamos nosso gerenciador de latout
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this,  LinearLayoutManager.HORIZONTAL, false);

        //setamos nosso gerenciados de layouts para nosso recycler view
        recyclerView.setLayoutManager(layoutManager);
        // passamos nossos dados para uma lista local para podermos utilizar nosso Up /Down
        data = photoList;
        //por fim, setamos nosso adapter ja com nossos dados para o recyclerview
        recyclerView.setAdapter(adapter);
        //caso seja necessario alterar os dados de nossa lista
        //podemos fazer uma funcao dentro de nosso DataPhotoAdapter
        //que realiza a edicao necessaria e chamamos a funcao notifyDataChange()
    }



    //as configucaçoes de nosso BottomNavigationView
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_up: {
                menuItem.setChecked(true);
                Toast.makeText(MainActivity.this, "UP", Toast.LENGTH_SHORT).show();
                recyclerView.scrollToPosition(0);
                break;
            }
            case R.id.nav_down: {
                menuItem.setChecked(true);
                Toast.makeText(MainActivity.this, "DOWN", Toast.LENGTH_SHORT).show();
                recyclerView.scrollToPosition(data.size() - 1);
                break;
            }
        }
        return false;
    }
}
