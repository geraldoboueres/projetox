package com.nanx.projectx.service;

import com.nanx.projectx.object.Data;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;



public interface FakeService {

    //nossa classe fakeservice possui os endpoints que podemos utilizar
    @GET("/photos")
    Call<List<Data>> listPhotos();

    //podemos ter multiplos endpoints
//    @GET("/links")
//    Call<List<Links>> listLinks();

//    @GET("/etcs")
//    Call<List<Etcs>> listEtcs();
}